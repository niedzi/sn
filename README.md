# BACKEND TEST IMPLEMENTATION

API endpoint on port 3000
KIBANA on port 5601

rest services on basic ports

Useful commands:

* docker-compose exec web rake routes
* docker-compose exec web rails console
* docker-compose exec web rake db:migrate

Initial db setup:

* docker-compose exec web rails db:migrate
* docker-compose exec web rails db:seed

# examlpe aggregation query to test in kibana dev tool

* 1 - sum

* GET products/_search
    {
    "query": {
      "match_all": {}
    },
    "size": "0",
    "aggs": {
      "quantity": {
        "nested": {"path": "variants"},
        "aggs": {
          "quantity_sum": {
            "sum": {
              "field": "variants.quantity"
            }
          }
        }
      }
    }
    }

* 2 - stats

* GET products/_search
    {
      "query": {
        "match_all": {}
      },
      "size": "0",
      "aggs": {
        "quantity": {
          "nested": {"path": "variants"},
          "aggs": {
            "stats": {
              "stats": {
                "field": "variants.quantity"
              }
            }
          }
        }
      }
    }