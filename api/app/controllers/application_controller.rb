class ApplicationController < ActionController::API

  protected

  def set_pagination_meta object
    {
        total_pages: object.total_pages,
        current_page: object.current_page,
        prev_page: object.prev_page,
        next_page: object.next_page,
        out_of_range: object.out_of_range?
    }
  end

end
