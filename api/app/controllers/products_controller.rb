class ProductsController < ApplicationController

  def index
    @products = Product.all.page(params[:page] || 1).per(params[:per_page] || KaminariConfig::PER_PAGE)
    options = {}
    options[:meta] = set_pagination_meta @products
    render json: ProductsSerializer.new(@products, options).serialized_json
  end

  def show
    @product = Product.find_by_slug(params[:id])
    options = {}
    render json: ProductSerializer.new(@product, options).serialized_json
  end

end
