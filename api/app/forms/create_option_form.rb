class CreateOptionForm
  include ActiveModel::Model

  attr_accessor :variant_id,
                :name, :value

  validates :name, presence: true
  validates :value, presence: true

  def save
    return false unless valid?

    property_name = Option.find_or_create_by(name: name)
    VariantOption.create(variant_id: variant_id, option_id: property_name.id, value: format_value)
  end

  private

  def format_value
    value.downcase
  end
end