class CreateProductForm
  include ActiveModel::Model

  attr_accessor :name, :variants, :supplier, :category
  attr_reader :options

  validates :name, presence: true
  validates :variants, presence: true
  validate :check_options

  def initialize params = {}
    @variants = []
    @supplier = nil
    @category = nil
    super
  end

  def save
    return false unless valid?

    ActiveRecord::Base.transaction do
      product = Product.create(name: name, slug: set_slug, supplier_id: supplier, category_id: category)
      variants.each do |variant|
        variant_form = CreateVariantForm.new(**variant, product: product, supplier: set_supplier)
        raise ActiveRecord::Rollback unless variant_form.save
      end
    end
  end

  private

  def check_options
    # check if passed options have same amount of options for each variants
    errors.add(:options, :wrong_amount) if variants.map do |v|
      v.fetch(:options, []).count
    end.uniq.count > 1

    #checking if options are unique
    errors.add(:options, :not_unique) if variants.map do |v|
      v.fetch(:options, [])
    end.map { |options| options.sort_by{ |k| k.fetch(:name) }.map{ |o| o.fetch(:value, '') }.join('') }.uniq.count != variants.count

    #checking if options are same for all variants
    errors.add(:options, :wrong_options) if variants.map do |v|
      v.fetch(:options, [])
    end.map { |options| options.sort_by{ |k| k.fetch(:name) }.map{ |o| o.fetch(:name, '') }.join('') }.uniq.count == variants.count
  end

  def set_supplier
    return nil unless supplier.present?
    supplier_obj = Supplier.find_by(id: supplier)
    supplier_obj.present? ? supplier_obj : nil
  end

  def set_slug
    slug = name.gsub(/\s+/, '-').downcase
    if Product.where(slug: slug).exists?
      loop do
        slug = "#{slug}-#{SecureRandom.hex(3)}".downcase
        break unless Product.where(slug: slug).exists?
      end
    end
    slug
  end

end