class CreatePropertyForm
  include ActiveModel::Model

  attr_accessor :variant_id,
                :name, :value, :show

  validates :name, presence: true
  validates :value, presence: true

  def initialize params = {}
    @show = true
    super
  end

  def save
    return false unless valid?

    property_name = Property.find_or_create_by(name: name)
    VariantProperty.create(variant_id: variant_id, property_id: property_name.id, value: format_value, show: show)
  end

  private

  def format_value
    value.downcase
  end
end