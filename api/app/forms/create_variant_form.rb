class CreateVariantForm
  include ActiveModel::Model

  attr_accessor :product, :properties, :options, :supplier,
                :base_sku, :cost, :msrp_price, :price, :sale_price,
                :sale_date_start, :sale_date_end, :quantity, :description, :main, :estimated_arrival

  validates :product, presence: true

  validates :base_sku, presence: true
  validates :price, presence: true

  def initialize params = {}
    @properties = []
    @options = []
    @main = false
    super
  end

  def save
    return false unless valid?

    variant = Variant.create(product_id: product.id,
                             base_sku: base_sku, sku: set_sku,
                             cost: cost, msrp_price: msrp_price, price: price, sale_price: sale_price,
                             sale_date_start: sale_date_start, sale_date_end: sale_date_end,
                             quantity: quantity, description: description,
                             main: main, estimated_arrival: estimated_arrival)

    options.each do |option|
      CreateOptionForm.new(**option, variant_id: variant.id).save
    end

    properties.each do |property|
      CreatePropertyForm.new(**property, variant_id: variant.id).save
    end

    true
  end

  private

  def set_sku
    supplier.present? ? "#{supplier.sku_prefix}#{base_sku}" : "SN#{base_sku}"
  end

end