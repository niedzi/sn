class Product < ApplicationRecord
  include Elasticsearch::Model

  # will be removed later and moved to sidekiq worker for async index
  include Elasticsearch::Model::Callbacks

  has_many :variants, dependent: :destroy
  has_many :variant_options, through: :variants

  belongs_to :supplier, required: false
  belongs_to :category, required: false

  mapping do
    indexes :id, type: 'keyword'
    indexes :name, type: 'text'
    indexes :slug, type: 'text'
    indexes :created_at, type: 'date'
    indexes :updated_at, type: 'date'
    indexes :variants, type: 'nested' do
      indexes :id, type: 'keyword'
      indexes :sku, type: 'text'
      indexes :price, type: 'float'
      indexes :sale_price, type: 'float'
      indexes :sale_date_start, type: 'date'
      indexes :sale_date_end, type: 'date'
      indexes :quantity, type: 'integer'

      indexes :variant_options, type: 'nested' do
        indexes :id, type: 'keyword'
        indexes :value, type: 'keyword'

        indexes :option, type: 'nested' do
          indexes :id, type: 'keyword'
          indexes :name, type: 'keyword'
        end
      end

      indexes :variant_properties, type: 'nested' do
        indexes :id, type: 'keyword'
        indexes :value, type: 'text'

        indexes :property, type: 'nested' do
          indexes :id, type: 'keyword'
          indexes :name, type: 'keyword'
        end
      end

    end
  end

  def as_indexed_json(options = {})
    as_json(only: ['id', 'name', 'slug', 'created_at', 'updated_at'],
            include: {
                variants: {
                  only: ['id', 'sku', 'price', 'sale_price', 'sale_date_start', 'sale_date_end', 'quantity'],
                  include: {
                      variant_properties: {
                          only: ['id', 'value'],
                          include: {
                            property: {
                                only: :name
                            }
                          }
                      },
                      variant_options: {
                          only: ['id', 'value'],
                          include: {
                              option: {
                                  only: :name
                              }
                          }
                      }
                  }
                }
            })
  end

end