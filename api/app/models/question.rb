class Question < ApplicationRecord
  belongs_to :questionable, polymorphic: true

  has_one :answer
end