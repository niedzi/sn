class RelatedVariant < ApplicationRecord
  belongs_to :variant, foreign_key: :variant_a_id
  belongs_to :variant, foreign_key: :variant_b_id
end