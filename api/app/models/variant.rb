class Variant < ApplicationRecord
  belongs_to :product

  has_many :variant_properties, dependent: :destroy
  has_many :variant_options, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :questions, as: :questionable, dependent: :destroy

  has_and_belongs_to_many(:related_variants_association,
                          class_name: "Variant",
                          join_table: "related_variants",
                          foreign_key: "variant_a_id",
                          association_foreign_key: "variant_b_id")

  has_and_belongs_to_many(:reverse_related_variants_association,
                          class_name: "Variant",
                          join_table: "related_variants",
                          foreign_key: "variant_b_id",
                          association_foreign_key: "variant_a_id")

  def related_variants
    (related_variants_association + reverse_related_variants_association).uniq
  end

end