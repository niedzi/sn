class VariantOption < ApplicationRecord
  belongs_to :variant
  belongs_to :option
end