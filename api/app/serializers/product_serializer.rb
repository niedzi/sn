class ProductSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :slug

  attribute :variants do |product|
    product.variants.map do |variant|
      variant_properties = variant.variant_properties.joins(:property).select("variant_properties.id, properties.name, variant_properties.value")
      variant_options = variant.variant_options.joins(:option).select("variant_options.id, options.name, variant_options.value")
      related_variants = variant.related_variants

      variant.attributes.merge(variant_properties: variant_properties, variant_options: variant_options, related_variants: related_variants)
    end
  end

end
