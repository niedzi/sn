class ProductsSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :slug
end
