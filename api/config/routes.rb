Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  namespace :admin do

  end

  resources :products, only: [:show, :index]
  resources :variants, only: [:show]

end
