class CreateProduct < ActiveRecord::Migration[5.2]
  def change
    create_table :products, id: :uuid do |t|
      t.string :name
      t.string :slug
      t.text :warranty
      t.text :shipment_and_return

      t.uuid :supplier_id
      t.uuid :category_id

      t.datetime :deleted_at
      t.timestamps
    end

    add_index :products, :name
    add_index :products, :slug
  end
end
