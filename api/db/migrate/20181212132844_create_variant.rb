class CreateVariant < ActiveRecord::Migration[5.2]
  def change
    create_table :variants, id: :uuid do |t|
      t.uuid :product_id

      t.string :base_sku
      t.string :sku

      t.decimal :cost, precision: 10, scale: 2
      t.decimal :msrp_price, precision: 10, scale: 2
      t.decimal :price, precision: 10, scale: 2
      t.decimal :sale_price, precision: 10, scale: 2

      t.datetime :sale_date_start
      t.datetime :sale_date_end

      t.boolean :backorderable, default: false
      t.integer :quantity
      t.text :description
      t.boolean :main, default: false
      t.datetime :estimated_arrival

      t.timestamps
    end

    add_index :variants, :sku
    add_index :variants, :product_id

  end
end