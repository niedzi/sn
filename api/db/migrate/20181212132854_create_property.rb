class CreateProperty < ActiveRecord::Migration[5.2]
  def change
    create_table :properties, id: :uuid do |t|
      t.string :name

      t.timestamps
    end

    add_index :properties, :name

  end
end
