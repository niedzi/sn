class CreateVariantProperty < ActiveRecord::Migration[5.2]
  def change
    create_table :variant_properties, id: :uuid do |t|
      t.uuid :variant_id
      t.uuid :property_id

      t.string :value
      t.boolean :show, default: true

      t.timestamps
    end

    add_index :variant_properties, :variant_id
    add_index :variant_properties, :property_id

  end
end
