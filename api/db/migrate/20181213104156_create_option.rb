class CreateOption < ActiveRecord::Migration[5.2]
  def change
    create_table :options, id: :uuid do |t|
      t.string :name

      t.timestamps
    end

    add_index :options, :name
  end
end
