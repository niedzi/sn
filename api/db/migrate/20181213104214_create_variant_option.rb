class CreateVariantOption < ActiveRecord::Migration[5.2]
  def change
    create_table :variant_options, id: :uuid do |t|
      t.uuid :variant_id
      t.uuid :option_id

      t.string :value

      t.timestamps
    end

    add_index :variant_options, :variant_id
    add_index :variant_options, :option_id

  end
end
