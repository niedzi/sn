class CreateReview < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews, id: :uuid do |t|
      t.uuid :variant_id

      t.string :reviewer_name
      t.text :content
      t.integer :rating

      t.timestamps
    end

    add_index :reviews, :variant_id
  end
end
