class CreateSupplier < ActiveRecord::Migration[5.2]
  def change
    create_table :suppliers, id: :uuid do |t|
      t.string :name
      t.string :email
      t.string :url
      t.string :sku_prefix

      t.timestamps
    end

    add_index :suppliers, :name

  end
end
