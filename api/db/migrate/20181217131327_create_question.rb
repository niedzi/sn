class CreateQuestion < ActiveRecord::Migration[5.2]
  def change
    create_table :questions, id: :uuid do |t|
      t.uuid :questionable_id
      t.string :questionable_type

      t.boolean :accepted, default: false
      t.string :from
      t.text :content

      t.timestamps
    end

    add_index :questions, :questionable_id
    add_index :questions, :questionable_type

  end
end
