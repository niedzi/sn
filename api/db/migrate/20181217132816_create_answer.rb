class CreateAnswer < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.uuid :question_id

      t.boolean :approved, default: false
      t.text :content

      t.timestamps
    end

    add_index :answers, :question_id

  end
end
