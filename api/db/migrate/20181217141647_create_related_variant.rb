class CreateRelatedVariant < ActiveRecord::Migration[5.2]
  def change
    create_table :related_variants, id: :uuid do |t|
      t.uuid :variant_a_id
      t.uuid :variant_b_id
    end

    add_index :related_variants, :variant_a_id
    add_index :related_variants, :variant_b_id
  end
end
