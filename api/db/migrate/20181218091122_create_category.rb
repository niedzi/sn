class CreateCategory < ActiveRecord::Migration[5.2]
  def change
    create_table :categories, id: :uuid do |t|
      t.uuid :parent_id
      t.string :name
      t.string :permalink
    end

    add_index :categories, :parent_id
    add_index :categories, :name
    add_index :categories, :permalink
  end
end
