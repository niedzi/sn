# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

suppliers_data = []
(1..2).each do |index|
  suppliers_data << {
      name: "SUPPLIER#{index}",
      email: "supplier#{index}@suppler.com",
      url: "https://www.google.com/",
      sku_prefix: "SUP#{index}"
  }
end
suppliers_data.each do |supplier_data|
  Supplier.create(supplier_data)
end

['Krzeslo', 'Wielkie Krzeslo', 'Male Krzeslo'].each_with_index do |product_name, index|
  data = {
      name: product_name,
      supplier: index.odd? ? Supplier.all.sample.id : nil,
      variants: [
          {
              base_sku: "SKU#{index}",
              cost: 10 + index,
              msrp_price: 100 + index,
              price: 200 + index,
              sale_price: index.odd? ? 150 + index : nil,
              sale_date_start: Time.zone.now - 1.day,
              sale_date_end: Time.zone.now + 1.month,
              quantity: 5 + index,
              description: "OPIS DLA #{product_name}",
              properties: [
                  {
                      name: 'color',
                      value: 'red'
                  },
                  {
                      name: 'size',
                      value: 'custom'
                  }
              ],
              options: [
                  {
                      name: 'size',
                      value: 'XL'
                  },
                  {
                      name: 'color',
                      value: 'red'
                  }
              ]
          },
          {
              base_sku: "SKU#{index}",
              cost: 10 + index,
              msrp_price: 100 + index,
              price: 200 + index,
              sale_price: index.odd? ? 150 + index : nil,
              sale_date_start: Time.zone.now - 1.day,
              sale_date_end: Time.zone.now + 1.month,
              quantity: 5 + index,
              description: "OPIS DLA #{product_name}",
              properties: [
                  {
                      name: 'size',
                      value: 'red'
                  },
                  {
                      name: 'size',
                      value: 'custom'
                  }
              ],
              options: [
                  {
                      name: 'color',
                      value: 'red'
                  },
                  {
                      name: 'size',
                      value: 'S'
                  }
              ]
          }
      ]
  }
  p = CreateProductForm.new(data)
  p.save
end

(1..10).each do |i|
  v_a_id = Variant.all.sample.id
  v_b_id = Variant.all.sample.id
  RelatedVariant.create(variant_a_id: v_a_id, variant_b_id: v_b_id) if v_a_id != v_b_id
end