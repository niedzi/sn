import Vue from 'vue';
import Application from '@/layouts/Application.vue';
import router from '@/router/index';
import store from '@/store/index';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(Application),
}).$mount('#app');
