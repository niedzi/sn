import { mapMutations, mapGetters } from 'vuex';

export default {
  computed: {
    ...mapGetters('about', ['counter']),
  },
  methods: {
    ...mapMutations('about', ['increment', 'decrement']),
  },
};
