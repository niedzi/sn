import Vue from 'vue';
import Vuex from 'vuex';

import createLogger from 'vuex/dist/logger';

import about from '@/store/modules/about';

import storeLogger from './plugins/storeLogger';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    about,
  },
  plugins: [storeLogger, createLogger()],
  // strict: true only on devel
});
