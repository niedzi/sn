import * as types from '../mutationTypes';

const state = {
  counter: 0,
};

const getters = {
  [types.COUNTER](state) { return state.counter; },
};

const mutations = {
  [types.INCREMENT](state) { state.counter++; },
  [types.DECREMENT](state) { state.counter--; },
};

const actions = {};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
