const storeLogger = (store) => {
  store.subscribe((mutation, state) => {
    if (process.env.NODE_ENV !== 'production') {
      console.log('storeLoggerPlugin');
      console.log(mutation);
      console.log(state);
    }
  });
};

export default storeLogger;
